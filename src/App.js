import React from 'react';
import './App.css';

class App extends React.Component {
   constructor(props) {
      super(props)
      this.state = {
         students: [
            { id: 1, name: 'ABC', age: 26, year: '2020' ,score:'100'},
            { id: 2, name: 'ABCD', age: 25, year: '2021',score:'60' },
            { id: 3, name: 'ABCDE', age: 20, year: '2019',score:'80' },
           
         ]
      }
   }

   renderTable() {
      let header = Object.keys(this.state.students[0])
      return header.map((key, index) => {
         return <th key={index}>{key}</th>
      })
   }

   renderdata() {
      return this.state.students.map((student, index) => {
         const { id, name, age, year,score } = student //destructuring
         return (
            <tr key={id}>
               <td>{id}</td>
               <td>{name}</td>
               <td>{age}</td>
               <td>{year}</td>
               <td>{score}</td>
            </tr>
         )
      })
   }

   render() {
      return (
         <div>
            <table className='students'>
               <tbody>
                  <tr>{this.renderTable()}</tr>
                  {this.renderdata()}
               </tbody>
            </table>
         </div>
      )
   }
}

export default App;


